import java.util.Arrays;

public class Calculations {
    public static int compute(int a, int b){
        return a*b+6;
    }

    public static double area(double radius){
        double res = 0.0;

        if (radius > 0)
            return Math.PI*radius*radius;
        return res;
    }

    public static boolean isPhoneNumber(String number){
        boolean result;
        if(number.length() != 8){
            result = false;
        }else{
            result = true;
        }
        return result;
    }

    public static int[] testSortedArray(int[] arr){
        Arrays.sort(arr);
        return arr;
    }
    public static int sum(int num){
        if(num <= 0){
            return 0;
        }else{
            return num + sum(num-1);
        }
    }

    public static int maxNumber(int num1, int num2, int num3){
        return Math.max(num1,Math.max(num2,num3));
    }
}
