import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class examCalculationTest {
    public static Calculations instance; // Refers to the original (production) code
    @org.junit.BeforeClass
    public static void setUp() throws Exception {
        System.out.println("Before Class");
        instance = new Calculations();
    }

    @org.junit.Before
    public void beforeTest(){
        System.out.println("Before test");
    }

    @org.junit.After
    public void tearDown() throws Exception {
        System.out.println("After unit test");
        instance=null;
    }
    @org.junit.AfterClass
    public static void afterClass(){
        System.out.println("After class");
    }


    @Test
    public void maxTest1(){
        System.out.println("testing for case1");
        int num1;
        int num2;
        int num3;
        num1 = 1;
        num2 = 23;
        num3 = 45;
        int expected = 45;
        int actual = instance.maxNumber(num1, num2, num3); //testing the non static instance here
        assertEquals(expected, actual);
    }

    @Test
    public void maxTest2(){
        System.out.println("testing for case2");
        int num1;
        int num2;
        int num3;
        num1 = -67;
        num2 = 7;
        num3 = -40;
        int expected = 7;
        int actual = Calculations.maxNumber(num1, num2, num3);
        assertEquals(expected, actual);
    }

    @Test
    public void maxTest3(){
        System.out.println("testing for case3");
        int num1;
        int num2;
        int num3;
        num1 = 8;
        num2 = 7;
        num3 = 7;
        int expected = 8;
        int actual = Calculations.maxNumber(num1, num2, num3);
        assertEquals(expected, actual);
    }

    @Test
    public void maxTest4(){
        System.out.println("testing for case4");
        int num1;
        int num2;
        int num3;
        num1 = 0;
        num2 = -7;
        num3 = -5;
        int expected = 0;
        int actual = Calculations.maxNumber(num1, num2, num3);
        assertEquals(expected, actual);
    }
}
