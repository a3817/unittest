import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class CalculationsTest {
    public static Calculations instance; // Refers to the original (production) code

    @org.junit.BeforeClass
    public static void setUp() throws Exception {
        System.out.println("Before Class");
        instance = new Calculations();
    }

    @org.junit.After
    public void tearDown() throws Exception {
        System.out.println("After unit test");
        instance=null;
    }
    @org.junit.AfterClass
    public static void afterClass(){
        System.out.println("After class");
    }

    @org.junit.Test
    public void computePositive() {
        System.out.println("testing compute... using parameters a=3 and b=8 must return 30!");
        int expected = 30;
        int actual = Calculations.compute(3,8);
        assertEquals(expected,actual);
    }

    @Test
    public void computeOneNegative() {
        System.out.println("testing compute... using one negative parameter, a=-6, and b=7 must return -36!");
        int expected = -36;
        int actual = Calculations.compute(-6,7);
        assertEquals(expected,actual);
    }

    // Exercise A: Write 2 new tests of the method compute!

    @Test
    public void areaZero() {
        System.out.println("testing area... using 0 as parameter must return 0");
        double expected = 0.0;
        double actual = Calculations.area(0);
        assertEquals(expected,actual,0.1F);

    }

    @Test
    public void areaFive(){
        System.out.println("testing area, using 5 as parameter must return 78.5");
        double expected = 78.5;
        double actual = Calculations.area(5);
        assertEquals(expected, actual, 0.1F);
    }

    @Test
    public void phoneTest1(){
        System.out.println("Testing phone number true");
        boolean actual = Calculations.isPhoneNumber("12345676");
        assertTrue(actual);
    }

    @Test
    public void phoneTest2(){
        System.out.println("Testing phone number false");
        boolean actual = Calculations.isPhoneNumber("3453456452");
        assertFalse(actual);
    }

    @Test
    public void arrayTest(){
        System.out.println("testing array");
        int[] testArray = {2,1,3,4};
        int[] expectedarray = {1,2,3,4};
        int[] actualArray = Calculations.testSortedArray(testArray);
        assertArrayEquals(expectedarray, actualArray);

    }


}